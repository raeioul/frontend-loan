import React from "react";
import "./App.css";
import axios from "axios";
import Form from 'react-bootstrap/Form'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      isLoading: true,
      errors: null
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getUsers() {
    axios
      .get("http://localhost:3000/users")
      .then(response => {
        this.setState({
          users: response.data,
          isLoading: false
        });
      })

      .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.getUsers();
  }

  handleSubmit = event => {
    event.preventDefault();
    const type = event.target.elements.type.value;
    if (type === "payment") {
      axios
        .post(" http://localhost:3000/payments", {
          email: event.target.elements.email.value,
          amount: event.target.elements.amount.value
        })
        .then(response => {
          console.log(response.data.data);
          this.setState({
            users: response.data.data,
            isLoading: false
          });
        })

        .catch(error => this.setState({ error, isLoading: false }));
    }
    if (type === "loan") {
      axios
        .post(" http://localhost:3000/loan", {
          email: event.target.elements.email.value,
          amount: event.target.elements.amount.value
        })
        .then(response => {
          this.setState({
            users: response.data.data,
            isLoading: false
          });
        })

        .catch(error => this.setState({ error, isLoading: false }));
    }
  };

  render() {
    const { isLoading, users } = this.state;
    return (
      <React.Fragment>
        <div className="App-background">
        <h2 className="App-link">Users/Loans</h2>
          <div className="App-list">
          {!isLoading ? (
            users.map(user => {
              const { _id, email, amount } = user;
              return (
                <div key={_id}>
                  <p>{email} : {amount}</p>
                  <hr />
                </div>
              );
            })
          ) : (
            <p>Loading...</p>
          )}
          </div>
        </div>
        <form onSubmit={this.handleSubmit} className="App-background-second">
          <Form.Group>
            <Form.Label>Email address:</Form.Label>
            <Form.Control type="email" placeholder="Enter email" name="email"/>
            <Form.Label>Amount:</Form.Label>
            <Form.Control type="text" placeholder="Amount" name="amount"/>
            <Form.Label>Type:</Form.Label>
            <div>
            <input type="radio" value="payment" name="type" /> Payment
            <input type="radio" value="loan" name="type" /> Loan
            </div>
            <button className="Button">Send data!</button>
          </Form.Group>
        </form>
      </React.Fragment>
    );
  }
}
